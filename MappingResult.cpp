#include "MappingResult.h"
CMappingResult::CMappingResult(void)
{
    this->initialization();
}

CMappingResult::CMappingResult(CAlignmentsQ& aQue, unsigned int uiReadLength = 0)
{
    initialization();
    this->uiReadLength = uiReadLength;
    this->uiDiff = aQue.MinDiff; //This is the minimum distance of mappings
    this->MultipleMappedNo = aQue.load;
    myStrCpy(this->QNAME, aQue.tag, FILENAME_MAX);
    // aQue.read.decode(this->caRead);
}

CMappingResult::~CMappingResult(void)
{
    ;
}

void CMappingResult::initialization(void)
{
    this->QNAME[0] = '\0';
    this->FLAG = 0;
    this->strand = '+';
    this->RNAME[0] = '\0';
    this->uiPOS = 0;
    this->MAPQ = 0;
    this->CIGAR[0] = '\0';
    this->MRNM[0] = '\0';
    this->uiMPOS = 0;
    this->ISIZE = 0;
    this->caRead[0] = '\0';
    this->QScores[0] = '\0';
    this->revComRead[0] = '\0';
    this->revQScores[0] = '\0';
    this->rawScores[0] = '\0';
    this->TAG[0] = '\0';
    this->SNPtype = ' ';
    this->caRef[0] = '\0';
    this->mismatchScore = 0;
    this->uiReadLength = 0;
    this->isColorRead = false;
    // this->uiDiff = 0; // TODO uncomment it
}

// currently not used
string get_diff_bases_bt_read_ref(const char* ref, const char* read)
{
    char diff_bases_list[MAX_LINE];
    vector<int> mis;
    for (unsigned int i = 0; read[i] != '\0'; i++) {
        if (read[i] != ref[i]) {
            mis.push_back(i+1);
        }
    }
    int j = 0;
    for (vector<int>::iterator it = mis.begin(); it != mis.end(); it++) {
        sprintf(&(diff_bases_list[j]), "%d_%c,", *it, ref[*it]);
        j += ((*it < 10) ? 3 : 4); // assume read length < 100
        if ( *it >= 100) j++;      // assume read length < 1000
    }
    return(string(diff_bases_list));
}
