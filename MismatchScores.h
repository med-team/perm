#pragma once

/*
 * A class used to do statistics in CReadInBitsSet for read mapping
 */
#include <iostream>
#include <fstream>
#include <limits.h>
#include <string.h>
using namespace std;

class CMismatchScores
{
public:
    CMismatchScores(void);
    CMismatchScores(unsigned int uiNoOfReads);
    ~CMismatchScores(void);
    int printArray(char* filename);
    int doStatistics(unsigned int uiNoOfReads, char* dataSet, int numOfSNP);
    // switch the function pointers to (1)Update records (2) Check if scores is the best alignment
    void switchUpdatesAndIsBest(bool firstRun, bool SOLiDRecord);

    // Function pointer point to the
    int (CMismatchScores::*update)(unsigned int, int);
    int callUpdate(unsigned int readId , int score);
    int normalUpdate(unsigned int readId , int score);
    int solidUpdate(unsigned int readId , int score); // special update for solid read mismatch score
    int dummyUpdate(unsigned int readId , int score);
    unsigned int uiNoOfReads;

    // return if an alignment is the best in record

    bool (CMismatchScores::*isBest)(unsigned int, int);
    bool callIsBest(unsigned int readId , int score);
    bool isBestInRecords(unsigned int readId , int score);
    bool isBestInRecords4Solid(unsigned int readId , int score);  // special record lookup for solid read mismatch score
    bool dummyBest(unsigned int readId , int score);

    // The array that record the best mapping of each reads.
    short* mismatchScore;
    // The array that record the number of identical best mapping of each reads.
    unsigned char* noOfBestMappings;
private:
    inline unsigned char addCounter(int readId);
    inline bool compareRecord4Solid(unsigned int readId , int score); // special record lookup for solid read mismatch score
};

