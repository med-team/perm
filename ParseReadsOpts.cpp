#include "ParseReadsOpts.h"

CParseReadsOpts::CParseReadsOpts(void)
{
    this->setDefaults();
}

CParseReadsOpts::~CParseReadsOpts(void)
{
}

void CParseReadsOpts::setDefaults(void)
{
    strcpy(this->readsFile, "");
    strcpy(this->qualityFile, "");
    // Reads files
    this->cFileFormatSymbol = 'N';
    this->truncatedReadLength = MAX_LINE;
    this->bDiscardReadWithN = true;
    this->allowedNumOfNinRead = 0;
    this->bMappedLongRead = false;
    this->bOddReadLengthAndLongRead = false;
    this->bMappedSOLiDRead = false;
}