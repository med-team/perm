#pragma once
// const unsigned int MAX_Q_CAPACITY = 2000;
const unsigned int MAX_Q_CAPACITY = 1000000;
const unsigned int MAX_TAG_LENGTH = FILENAME_MAX;
#include "ReadInBits.h"
#include <algorithm>
#include <vector>
/*
 * This class has changed to queue alignment we found, instead of hits
 * It records the hits start and the shift of the hits
 * It will optionally keep the best set (default), all or a single alignment record
 * pushed in according to the setting
*/
class CAlignmentsQ
{
public:
    CAlignmentsQ(unsigned int iMaxCapacity = MAX_Q_CAPACITY);
    CAlignmentsQ(char cFlag_of_Queue_All_Best_One, unsigned int iMaxCapacity = MAX_Q_CAPACITY);
    ~CAlignmentsQ(void);
    unsigned int iMaxCapacity;
    void setQueue_All_Best_OneFlag(char cFlag_of_Queue_All_Best_One);
    void setForwardLoad(bool forward) {
        if (forward) ForwardAlignmentLoad = load;
    };
    char returnQueue_All_Best_OneFlag();
    unsigned int topHitsinList();
    unsigned int saveHits(unsigned int startindex, unsigned short diff);
    int replaceHits(unsigned int startindex, unsigned short diff);
    inline void pushHits(unsigned int startindex, unsigned short diff);
    inline bool checkHits(unsigned int startindex);
    int clearHits();
    int sortHitsByLocation();
    int filterAlignments(unsigned int mismatchThreshold, bool bKeepAllAlignmentsInThreshold);
    inline bool qAllInThreshold(void) {
        return cFlag_of_Queue_All_Best_One == 'A';
    }

    static const unsigned int NULL_RECORD;
    static const unsigned short NULL_EDIT_DIS;

    int readID;
    CReadInBits read;
    char tag[MAX_TAG_LENGTH];
    const char* qualityScores;
    bool AmbiguousFlag;
    bool reverseIsBetter;
    // How many alignment is in the queue
    unsigned int load;
    // Assume the first proportion of records are forward alignments, and the later part is reverse compliment alignment
    unsigned int ForwardAlignmentLoad;
    unsigned int MinDiff;
    unsigned int* aiHitIndex;; // store the hit index
    unsigned short* asdiff; // store the alignment difference between the string
private:
    int initialization(unsigned int iMaxCapacity = MAX_Q_CAPACITY);
    // cFlag_of_Queue_All_Best_One is a flag that control whether we queue all alignment record,
    // or keep the best alignments set only. (best is defined by the # of substitutions).
    char cFlag_of_Queue_All_Best_One;
};

