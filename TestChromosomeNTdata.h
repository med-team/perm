#pragma once
#include "chromosomeNTdata.h"

class TestChromosomeNTdata
{
public:
    const static unsigned int defaultNtPerLine = 96;
    TestChromosomeNTdata(const char* testInputChrFileN, const char* testOutputChrFileN);
    ~TestChromosomeNTdata(void);
    int outputFasta(const char* filename, unsigned int ntPerLine, const char* ntStr);
    int generateTestInput(const char* filename);
};
