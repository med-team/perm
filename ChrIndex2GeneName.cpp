#include "stdafx.h"
#include "ChrIndex2GeneName.h"

CGene::CGene(void)
{
    ;
}
CGene::~CGene(void)
{
    ;
}

CGene::CGene(string name, unsigned int startIndex)
{
    this->name = name;
    this->startIndex = startIndex;
    this->isValid = true;
}

CGene::CGene(string name, unsigned int startIndex, bool isValid)
{
    this->name = name;
    this->startIndex = startIndex;
    this->isValid = isValid;
}

bool CGene::operator<(const CGene &other) const
{
    return (this->startIndex < other.startIndex);
}

bool CGene::operator==(const CGene &other) const
{
    return(other.name == this->name);
}

ChrIndex2GeneName::ChrIndex2GeneName(void)
{
    this->table.clear();
}

ChrIndex2GeneName::~ChrIndex2GeneName(void)
{

}

int ChrIndex2GeneName::insert(string name, unsigned int startIndex)
{
    CGene g(name, startIndex);
    this->table.push_back(g);
    return((int)this->table.size());
}

CGene ChrIndex2GeneName::query(unsigned int chrIndex)
{
    CGene g("search", chrIndex);
    vector<CGene>::iterator gIt = upper_bound(this->table.begin(), this->table.end(), g);
    if (gIt >= (this->table.begin() + 1) && (gIt - 1)->startIndex <= chrIndex) { // the gene before upper bound
        return(CGene((gIt - 1)->name, chrIndex - (gIt - 1)->startIndex, true));
    } else {
        return(CGene(string("Null_Region:"), chrIndex, false));
    }
}

