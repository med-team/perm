#pragma once
#include <iostream>
#include <deque>
#include <string.h>
using namespace std;

/**
 * This class is design to use a char to store 8 bool flags
 * as an alternative of vector<bool>. It is easy to dump and
 * read the content of this flag array by fwrite and fread.
 */
class CboolFlagArray
{
public:
    CboolFlagArray(void);
    CboolFlagArray(unsigned int size);
    ~CboolFlagArray(void);
    unsigned char* bflag;
    //The size is # of bits so the real size in bytes is size/8 + 1
    unsigned int size;
    // return true if the flag is set
    bool b(unsigned int index) const;
    // return true if there is a flag set within the widows
    bool b(unsigned int index, unsigned int windowLength) const;
    void setflag(unsigned int index, bool flag);
    unsigned int initialization(unsigned int size);
};

inline unsigned int size2sizeInByte(unsigned int size)
{
    return(size/8 + 1);
}

