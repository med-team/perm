#pragma once
#include "PairedReadsSet.h"
#include "FileOutputBuffer.h"

class CLongReadsSet : public CPairedReadsSet
{
public:
    CLongReadsSet(void);
    CLongReadsSet (const char* InputFile, const char* fileFormat,\
                   unsigned int expReadStrLineLength, unsigned int allowedNumOfNinRead, unsigned int readStartIndex);
    ~CLongReadsSet(void);
    unsigned int get_next_capacity_long_reads();
    unsigned int get_next_capacity_long_reads(CLongReadsSet &set2);
    bool save_next_long_read(bool bSOLiDReadFormat, bool getQScores, bool in5to3cat3to5Format);
    // bool save_next_long_read(unsigned int fullReadLength, bool getQScores, bool in5to3cat3to5Format);
    inline bool save_next_long_Illumina_read(unsigned int fullReadLength, bool getQScores, bool in5to3cat3to5Format);
    inline bool save_next_long_SOLiD_read(unsigned int fullReadLength, bool getQScores);
    int size();
    void setBadReadOutputFile(FileOutputBuffer* pOut);
    unsigned int longReadLength;
    friend int get_next_capacity_long_paired_reads(CLongReadsSet &set1, CLongReadsSet &set2);
};
int get_next_capacity_long_paired_reads(CLongReadsSet &set1, CLongReadsSet &set2);