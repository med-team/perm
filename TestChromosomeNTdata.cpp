#include "TestChromosomeNTdata.h"

TestChromosomeNTdata::TestChromosomeNTdata(const char* testInputChrFileN, const char* testOutputChrFileN)
{
    cout << "Start TestChromosomeNTdata" << endl;
    bool bFasta = true;
    unsigned int ntPerLine = 96; // TODO manually test
    generateTestInput(testInputChrFileN);
    cout << "generateTestInput" << endl;
    CchromosomeNTdata* chr = new CchromosomeNTdata(testInputChrFileN, bFasta);
    cout << "Got Chr" << endl;
    outputFasta(testOutputChrFileN, ntPerLine, chr->caChromosome);
    cout << "end" << endl;
    delete chr;
}

TestChromosomeNTdata::~TestChromosomeNTdata(void)
{
}

int TestChromosomeNTdata::outputFasta(const char* filename, unsigned int ntPerLine, const char* ntStr)
{
    int lineCounter = 0;
    ofstream ofile(filename);
    int length = (int)strlen(ntStr);
    for(int ntCount = strlen(ntStr); ntCount > 0; ntCount = ntCount - ntPerLine) {
        for(int i = 0; i < ntPerLine; i++) {
            int index = lineCounter * ntPerLine + i;
            char c = ntStr[index];
            if( c != EOF && c != '\0' && index != length) {
                ofile << c;
            } else {
                ofile << "\n";
                break;
            }
        }
        ofile << endl;
        lineCounter ++;
    }
    ofile.close();
    return(0);
}

int TestChromosomeNTdata::generateTestInput(const char* filename)
{
    // defaultNtPerLine = 48;
    int lineNo = 500000;
    const char* line = "ACGTNNAnaCGTNNCnACGTNNGnAcGTNNTnACGTNNAnACgTNNCnACGtNNGnaCGTNNTnAcGTNNAnACgTNNCnACgTNNGnACGtNNTn";
    ofstream ofile(filename);
    ofile << ">Test Ref" << endl;
    for(int i = 0; i < lineNo; i++) {
        ofile << line << endl;
    }
    ofile.close();
    return(0);
}