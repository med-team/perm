#include "stdafx.h"
#include "HashIndexT.h"

const unsigned int CHashIndexT::INDEX_BASES_LIMIT = 13; //Maximum 64M index array

CHashIndexT::CHashIndexT(void)
{
    this->initialization(0);
}
CHashIndexT::CHashIndexT(unsigned int uiBucketSize)
{
    this->initialization(uiBucketSize);
}

CHashIndexT::~CHashIndexT(void)
{
    // cout << "Free a " << this->uiSize << "Hash Index table" << endl;
    delete [] this->aiIndexTable;
    this->aiIndexTable = NULL;
}

int CHashIndexT::initialization(unsigned int BucketSize)
{
    unsigned int i;
    if (BucketSize > 0) {
        i = BucketSize - 1;// 1024 map to 5 1023 map to 5, 1025 map to 6
        for (uiWindowSize = 0; i > 0; uiWindowSize++) {
            i >>= 2;
        } // Get the number of base pair need to use as hashkey

        if (uiWindowSize > INDEX_BASES_LIMIT)
            uiWindowSize = INDEX_BASES_LIMIT;

        this->uiSize = (unsigned int) pow(4.0, (double)uiWindowSize);
        LOG_INFO("Info %d: Allocate a %u Hash Index table.\r", FINE_LOG, this->uiSize);
    } else {
        uiSize = 0;
    }

    if (uiSize > 0) {
        //One more record at the end, after filling the table, the record will shift 1
        this->aiIndexTable = new unsigned int [uiSize + 1];
        memset(this->aiIndexTable, 0x00, sizeof(unsigned int) *(this->uiSize + 1));
    } else
        this->aiIndexTable = NULL;
    return(0);
};

int CHashIndexT::Counter2Index(void)
{
    unsigned int i;
    aiIndexTable[uiSize] = 0;
    for (i = 1; i <= this->uiSize; i++) {
        aiIndexTable[i] += aiIndexTable[i-1];
    }
    return(0);
}

