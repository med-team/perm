#pragma once
#include "stdafx.h"

class CParseReadsOpts
{
public:
    CParseReadsOpts(void);
    virtual ~CParseReadsOpts(void);
    void setDefaults(void);
    char readsFile[FILENAME_MAX];
    char qualityFile[FILENAME_MAX];
    char cFileFormatSymbol;
    // Reads files (can be selected by reads file)
    bool bDiscardReadWithN;           // Default is true
    bool bMappedSOLiDRead;
    bool bMappedLongRead;
    bool bOddReadLengthAndLongRead;
    unsigned int truncatedReadLength;
    unsigned int allowedNumOfNinRead;
};
