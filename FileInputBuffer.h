#pragma once

#ifdef WIN32
const unsigned int MAX_INPUT_BUFFER_SIZE = 10000;
#else
const unsigned int MAX_INPUT_BUFFER_SIZE = 1000000;
#endif

#include <string>
#include <iostream>
using namespace std;

class FileInputBuffer
{
public:
    FileInputBuffer(void);
    FileInputBuffer(unsigned int uiCapacity, ifstream* pifile);
    ~FileInputBuffer(void);

    unsigned int uiCapacity;
    unsigned int uiPtrIndex; //Index to point out the next char to read
    unsigned int Getline(char* caArray, unsigned int uiMax_Char_Per_Line); //return the char get
    void initialize(unsigned int uiCapacity, ifstream* pifile);
    void fflush(void);
    bool ready2Read(void);
private:
    char* caBuffer;
    char* caBufp;
    filebuf *pbuf;
};
unsigned long long getFileSize(const char* fileName);
unsigned long long getNumberOfLineInAFile(const char* fileName);


