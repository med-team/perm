#include "minQ.h"
#include <iostream>


minQ::minQ(void)
{
}


minQ::~minQ(void)
{
}

int minQ::push(int num)
{
    this->q.push(num);
    while(this->min.size() > 0 && num < this->min.back()) {
        this->min.pop_back();
    }
    this->min.push_back(num);
    return(this->q.size());
}
int minQ::pop()
{
    if(this->q.size() > 0) {
        int front = this->q.front();
        this->q.pop();
        if(this->min.front() == front) {
            this->min.pop_front();
        }
    } else {
        cout << "empty que" << endl;
    }
    return(this->q.size());
}
int minQ::front()
{
    return(this->q.front());
}
int minQ::getMin()
{
    return(this->min.front());
}

void testMinQ()
{
    minQ q;
    q.push(2);
    cout << q.getMin() << endl; // 2
    q.push(1);
    cout << q.getMin() << endl; // 1
    q.push(3);
    q.pop();
    cout << q.getMin() << endl; // 1
    q.push(0);
    q.pop();
    cout << q.getMin() << endl; // 0
}

