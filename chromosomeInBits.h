#pragma once
#ifndef CChromosomeInBits_H_
#define CChromosomeInBits_H_

#include "bitsOperationUtil.h"
#include "SeedPattern.h"
#include "ReadInBits.h"

class CChromosomeInBits
{
public:
    CChromosomeInBits(void);
    CChromosomeInBits(char* caChromosome, unsigned int uiChrLength);
    ~CChromosomeInBits(void);
    // Two arrays to store the chromosome string encoded with bits
    WORD_SIZE* pUpperBits;
    WORD_SIZE* pLowerBits;
    unsigned int uiChrLength;
    unsigned int uiChrLengthInWordSize;
    char* caChromosome; // Don't delete this pointer pointing outside
    // get the wordSize substring encoded in bits and store in upperBits and lowerBits
    CReadInBits getSubstringInBits(unsigned int uiGenomeIndex);
    // eliminate the bits beyond read length. Not the length should smaller than word_size
    CReadInBits getSubstringInBits(unsigned int uiGenomeIndex, unsigned int uiSubstringLength);
    // call getSubstringInBits and transform the info to DNA sequence and store in caSubstring
    char* getSubstring(unsigned int uiGenomeIndex);
    // get the substring encoded in bits which is shorter than wordSizea.
    char* getSubstring(unsigned int uiGenomeIndex, unsigned int uiSubstringLength);
    char caSubstring[128 + 1];
    //WORD_SIZE upperBits;
    //WORD_SIZE lowerBits;
private:
    int initialization(void);
};
#endif

