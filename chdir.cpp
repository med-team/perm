#include "chdir.h"
#ifdef _WIN32
#include <direct.h>
#else
#ifdef _WIN64
#include <direct.h>
#else
#define _chdir chdir
#define _getcwd getcwd
#include <unistd.h>
#endif
#endif

int goto_working_directory(const char* path)
{
    int returnV;
    if (path[0] == '\0') {
        returnV = -1;
    } else {
        returnV = tosomedir(path);
    }
    /** currently no warning message
    if(returnV == -1) {
    	cout << "Stay in the current directory" << endl;
    	print_working_directory();
    } */
    return(returnV);
}

char* get_working_directory(char* path)
{
    if (_getcwd(path, FILENAME_MAX) == NULL) {
        perror("_getcwd error");
        path[0] = '\0';
    }
    return(path);
}

string get_working_directory(void)
{
    char path[FILENAME_MAX];
    if (get_working_directory(path)) {
        return(string(path));
    }
    return(string(""));
}

bool is_accessible_directory(const char * path)
{
    bool isAccessibleDir = false;
    string cwd = get_working_directory();
    if (goto_working_directory(path) == 0) {
        isAccessibleDir = true;
    }
    // go back to original dir
    goto_working_directory(cwd.c_str());
    return(isAccessibleDir);
}


int print_working_directory(void)
{
    char path[FILENAME_MAX];

    if (get_working_directory(path)) {
        cout << "\nThe working directory is " << path << endl;
    }
    return(0);
}

int toparentdir(void)
{
    int i, l;
    char path[FILENAME_MAX];

    if (_getcwd(path, FILENAME_MAX) == NULL)
        perror("_getcwd error");

    l = (int)strlen(path);
    for (i = l - 1; i >= 0; i--) {
#ifdef WIN32
        if (path[i] == '\\')
#else
        if (path[i] == '/')
#endif
            break;
        else if (path[i] == ':') {
            printf("Already in the root");
            return(-1);
        }
    }
    path[i] = '\0';

    if (_chdir(path)) {
        printf("Unable to locate the directory: %s\n", path);
        return(-1);
    } else if (path[i-1] == ':') {
        printf("Disk root");
        return(0);// in the root
    } else {
        return(1);
    }
}

int tochilddir(const char* dirname)
{
    int l;
    char path[FILENAME_MAX];

    if (_getcwd(path, FILENAME_MAX) == NULL)
        perror("_getcwd error");

    l = (int)strlen(path);
#ifdef WIN32
    path[l] = '\\';
#elif defined _WIN64
    path[l] = '\\';
#else
    path[l] = '/';
#endif
    strcpy(&path[l+1], dirname);

    if (_chdir(path)) {
        printf("Unable to locate the directory: %s\n", path);
        return(-1);
    } else {
        return(system("dir *.wri"));
    }
}

int tosomedir(const char* path)
{
    if (_chdir(path)) {
        // printf("Unable to locate the directory: %s\n", path);
        return(-1);
    } else {
        return(0);
    }
}

int tosiblingdir(const char* dirname)
{
    toparentdir();
    tochilddir(dirname);
    return(0);
}

int str2Int(char* str)
{
    int value;
    int i, j; //j is a value of a digit;

    for (value = 0, i = 0; (str[i] != '\0'); i++) {
        j = (int)(str[i] - '0');
        if (j >= 0 && j <= 9) {
            value = value * 10 + j;
        } else {
            return(-1);
        }
    }
    return(value);
}

string pathLize(const char* path) //For windows
{
    char dirPath[FILENAME_MAX];
#ifdef WIN32
    strcpy(dirPath, path);
#elif defined _WIN64
    strcpy(dirPath, path);
#else
    if (path[0] == '/')
        strcpy(dirPath, path);
    else
        sprintf(dirPath, "./%s", path);
#endif
    return(string(dirPath));
}

int createdir(const char* path)
{
    struct stat st;
#ifdef WIN32
    if ( stat(path, &st) == 0 || \
            _mkdir(path) == 0) {
#else
#ifdef _WIN64
    if ( stat(path, &st) == 0 || \
            _mkdir(path) == 0) {
#else
#define MODE_MASK 0777
    string dirPath = pathLize(path);
    if ( stat(dirPath.c_str(), &st) == 0 || \
            mkdir(dirPath.c_str(), MODE_MASK ) == 0) {
#endif
#endif
        return(0);
    } else {
        printf("Fail to create the dir %s", path);
        return(1);
    }
}

int deletedir(const char* path)
{
    char systemcomand[FILENAME_MAX];
    sprintf(systemcomand, "rmdir %s", path);
    return(system(systemcomand));
}
int deletefile(const char* path)
{
    char systemcomand[FILENAME_MAX];
    sprintf(systemcomand, "del %s", path);
    return(system(systemcomand));
}
char* getnamefrompath(char* Path, char* filename)
{
    int i;

    for (i = (int)strlen(Path); i >= 0; i--) {

#ifdef WIN32
        if (Path[i] == '\\')
#else
        if (Path[i] == '/')
#endif
        {
            break;
        }
    }
    strcpy(filename, &(Path[i+1]));
    for (i = (int)strlen(filename); i > 0; i--) {
        if (filename[i] == '.') {
            filename[i] = '\0';
        }
    }
    return(filename);
}


