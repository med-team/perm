CFLAGS =  -ggdb -Wall -fopenmp -static
CC = g++ -O2 $(CFLAGS)

TARGETS = perm
LIBS = -lm -lstdc++ 
 
PER_M = AlignmentsQ.cpp Filename.cpp GenomeNTdata.cpp ReadInBits.cpp PerM.cpp chromosomeNTdata.cpp\
bitsOperationUtil.cpp FileOutputBuffer.cpp HashIndexT.cpp ReadInBitsSet.cpp SeedPattern.cpp\
boolFlagArray.cpp GenomeInBits.cpp Index_Table.cpp ReadsMapping.cpp PairedReadsMapping.cpp  ShortReadUtil.cpp\
chdir.cpp Genome_Index.cpp MismatchScores.cpp stdafx.cpp Flags.cpp ParseReadsOpts.cpp\
ColorSpaceRead.cpp Genome_Index_Table.cpp ParameterList.cpp ReadsMappingStats.cpp\
FileInputBuffer.cpp Genome_Index_TableQ.cpp ReadsQualScores.cpp ChrIndex2GeneName.cpp\
ReadsFileParser.cpp PairedReadsSet.cpp MappingResult.cpp refInBinFile.cpp LongReadsSet.cpp

all:		$(TARGETS)
#	-ctags *.[ch]


install:	all
	strip $(TARGETS)
	cp $(TARGETS) /usr/local/sbin
	cp *.1 /usr/local/man/man1


perm:	$(PER_M)
	make clean
	$(CC) -o $@ $(CFLAGS) $(LIB_PATH) $(PER_M) $(LIBS)
	#$(CC) -o $@ $(LIB_PATH) *.o $(LIBS)

tar:	clean
	tar cvfz $(TARGETS).tar.gz *.cpp *.h makefile

clean:
	-rm -f *.o *.exe cut out $(TARGETS) $(TARGETS).tar.gz
	
