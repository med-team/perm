#pragma once
#include "chromosomeNTdata.h"
#include "GenomeInBits.h"
#include "GenomeNTdata.h"
#include "ShortReadUtil.h"
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits>
int readRefInBinFile(FILE* fp, CGenomeInBits* gInBits, CGenomeNTdata* g);
int saveRefInBinFile(FILE* fp, const CGenomeInBits* gInBits, const CGenomeNTdata* g);

