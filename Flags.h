#include "stdafx.h"
#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

// This class is designed to parse the options of each command line.
class CFlags
{
public:
    // The string vector to save all the flags that has been checked.
    vector<string> flags;
    bool checkArg(int argc, const char** argv, const char* arg);
    bool checkIntOpt(int argc, const char** argv, const char* arg, int& argValue);
    bool checkUnIntOpt(int argc, const char** argv, const char* arg, unsigned int& argValue);
    bool checkpCharOpt(int argc, const char** argv, const char* arg, char& argValue);
    bool checkpStrOpt(int argc, const char** argv, const char* arg, string& argStr);
    bool checkpStrOpt(int argc, const char** argv, const char* arg, char* argStr);
    // Output warning if there are any unrecognizable options in the command line
    bool checkUnrecognizedFlags(int argc, const char** argv);
};