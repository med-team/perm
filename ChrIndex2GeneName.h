#pragma once
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
#ifndef CHR_INDEX2GENE_NAME_H
#define CHR_INDEX2GENE_NAME_H

class CGene
{
public:
    CGene(void);
    ~CGene(void);
    CGene(string name, unsigned int startIndex);
    CGene(string name, unsigned int startIndex, bool isValid);

    string name;
    unsigned int startIndex;
    bool isValid;
    bool operator<(const CGene &other) const;  // compare the start index
    bool operator==(const CGene &other) const; // return true if the name is the same
};

class ChrIndex2GeneName
{
public:
    ChrIndex2GeneName(void);
    virtual ~ChrIndex2GeneName(void);
    int insert(string name, unsigned int startIndex);
    // translate the chromosome index to the gene it locates and the corresponding position in the gene
    CGene query(unsigned int chrIndex);
    vector<CGene> table;
};
#endif

