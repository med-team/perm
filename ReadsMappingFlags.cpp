#include "stdafx.h"
#include "ReadsMappingFlags.h"

CReadsMappingFlags::CReadsMappingFlags(void)
{
    this->set_Default_Opt();
}

CReadsMappingFlags::~CReadsMappingFlags(void)
{
}

int CReadsMappingFlags::set_Default_Opt(void)
{
// OUTPUT
    // Default is print all the 'best' alignments with the fewest substitutions.
    this->bSearchAllAlignment = false;
    this->bPrintUnMappedReads = false;
    this->bPrintGeneName = true;
    this->bPrintAlignment = true;
    this->cOutputFormat = 'm';
// PREPROCESSING
    this->bSaveTable = false;
    return(0);
}


