#pragma once
/*****
*	This class is a Index table for Hash Table.
*	When building the table, we hash
*	Before building the index, we will do a counting for the first run and set the index
*	When there is a record (Query), they will look up the index and bucket to a range and than do binary search
*/
class CHashIndexT
{
public:
    CHashIndexT(void);
    CHashIndexT(unsigned int uiBucketSize);
    ~CHashIndexT(void);

    unsigned int uiSize;
    unsigned int uiWindowSize;//The # of base we need to use as hash key
    unsigned int* aiIndexTable;//Just use a integer as index;
    int Counter2Index(void);
    static const unsigned int INDEX_BASES_LIMIT;
private:
    unsigned int HashFunction(char* Kmer) const;
    int initialization(unsigned int BucketSize);
};



