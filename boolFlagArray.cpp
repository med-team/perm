#include "boolFlagArray.h"

CboolFlagArray::CboolFlagArray(void)
{
    this->bflag = NULL;
    this->size = 0;
}

CboolFlagArray::~CboolFlagArray(void)
{
    delete [] this->bflag;
}
//Watch out the size is counted as bits. so size should be No of bits/8+1
CboolFlagArray::CboolFlagArray(unsigned int size)
{
    this->bflag = new unsigned char[size2sizeInByte(size)];
    this->initialization(size);
}
bool CboolFlagArray::b(unsigned int index) const//return the flag
{
    if (this->bflag != NULL && index < this->size) {
        int bitID = index % 8; //Save
        unsigned char c = this->bflag[index/8], d = 0x01;
        d = d << bitID;
        if (c & d)
            return(true);
        else
            return(false);
    } else
        return(0);
}

// if there is a flag set within the widows
bool CboolFlagArray::b(unsigned int index, unsigned int windowLength)  const
{
    for (unsigned int i = index; i < index + windowLength; i++) {
        if (this->b(i)) {
            return(true);
        }
    }
    return(false);
}

void CboolFlagArray::setflag(unsigned int index, bool flag)
{
    if (this->bflag != NULL && index < this->size) {
        int bitID = index % 8;
        unsigned char d = (0x01 << bitID);

        if (flag) {
            this->bflag[index/8] |= d;
        } else {
            d ^= 0xff; //Complement
            this->bflag[index/8] &= d;
        }
    } else
        cout << "Wrongly set Flag" << endl;
}

unsigned int CboolFlagArray::initialization(unsigned int size)
{
    this->size = size;
    //Default Setting of flags are 0 -> false
    memset(this->bflag, 0x00, sizeof(unsigned char)*(size2sizeInByte(this->size)));
    return(size2sizeInByte(this->size));
}


