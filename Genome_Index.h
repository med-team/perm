#pragma once
/*
 * This class is the base class of Genome_Index_Table. It contains the the CGenomeNTData and
 * the function pointers which generated the hashValue and SeedKey as index for each sliding windows
 */

#include "stdafx.h"
#include "GenomeNTdata.h"
#include "SeedPattern.h"
#include "ReadInBits.h"
#include "GenomeInBits.h"
#include "seedOptions.h"

const unsigned int MAX_MISMATCH_THRESHOLD = 10;
class CGenome_Index
{
public:
    CGenome_Index(void);
    virtual ~CGenome_Index(void);
    // HashValue is to located the bucket. SeedKey is for binary sort in the bucket
    unsigned int getHashValue(char* slide_window) const;
    unsigned int getSeedKey(char* slide_window) const;
    unsigned int getHashValue(CReadInBits r) const;
    unsigned int getSeedKey(CReadInBits r) const;

    char caRefName[FILENAME_MAX]; // Name from the input List
    CGenomeNTdata* pgenomeNT;
    CGenomeInBits* pgenomeNTInBits;
    bool bEXTEND_SEED;
    // Number of digits (bases or colors) used for hashing
    int iHashDigits;
    // Number of digits (bases or colors) used for generating key for binary search
    int iKeyDigits;
    // Should be  iHashDigits + iKeyDigits
    unsigned int uiSeedLength;
    // is the read length - uiSeedLength
    unsigned int uiNoOfShift;
    unsigned int NO_OF_BUCKET;
protected:
    // Set function pointers to generate hashvalue and SeedKey (using seed pattern)
    int chooseHashFunction(unsigned int uiReadLength, unsigned int chosenSeedId);
    void setConsecutiveHashFunction(void);
private:
    int initialization(void);
    int chooseSeedKeyFunction(unsigned int uiReadLength, unsigned int chosenSeedId);
    // fucntion pointers pointer
    unsigned int (* fpHashValue)(CReadInBits);
    unsigned int (* fpSeedKey)(CReadInBits, int);
};


