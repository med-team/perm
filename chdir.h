#ifndef CHDIR_H
#define CHDIR_H
#include "stdafx.h"
#include <ctype.h>
#include <string.h>
#include <sys/stat.h>

string get_working_directory(void);
char* get_working_directory(char* path);
int goto_working_directory(const char* path);
bool is_accessible_directory(const char * path);
int print_working_directory(void);
int toparentdir(void);
int tochilddir(const char* dirname);
int tosomedir(const char* path);
int tosiblingdir(const char* dirname);
int createdirsAlongPath(const char* path);
int createdir(const char* path);
int deletedir(const char* target);
int deletefile(const char* target);
int str2Int(const char* str);
char* getnamefrompath(char* Path, char* filename);

#endif

