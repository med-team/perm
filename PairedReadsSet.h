#pragma once
#ifndef CPAIRED_READ_SET_H_
#define CPAIRED_READ_SET_H_
#include "FileOutputBuffer.h"
#include "ReadsFileParser.h"
#include "ReadInBitsSet.h"
#include "ReadInBits.h"
#include "ColorSpaceRead.h"
#include "ShortReadUtil.h"
#include "MismatchScores.h"
#include "ReadsQualScores.h"
#include <vector>
#include <iostream>
#include <string>
using namespace std;

class CPairedReadsSet
{
public:
    CPairedReadsSet(void);
    CPairedReadsSet(unsigned int Capacity, unsigned int uiReadLength);
    // Generate set given a file with reads and readLength (must be known in advance)
    CPairedReadsSet(const char* InputFile, const char* fileFormat,\
                    unsigned int uiReadLength, bool in5to3cat3to5Format, unsigned int allowedNumOfNinRead, unsigned int readStartIndex = 0);
    virtual ~CPairedReadsSet();
    void ignoreQScores(void);
    // unsigned int get_next_capacity_reads_pairs_from_single_file(void);

    CReadInBitsSet* F_Reads;
    CReadInBitsSet* R_Reads;

    unsigned int allowedNumOfNinRead;
    unsigned int  uiRead_Length;
    unsigned int  uiNo_of_Reads;
    unsigned int  uiNo_of_Bad_Reads;
    bool in5to3cat3to5Format;
    // A char flag used to record the input file type, example F for fasta, S for .seq.txt, A for .realign
    char cFileType;
    char InputFile[FILENAME_MAX];
    // Keep the info of the quality score
    CReadsQualScores* pQualScoresF;
    CReadsQualScores* pQualScoresR;
protected:
    // get reads from the file and store (append) in a vector. Return how many reads are read-in.
    unsigned int openAFileReady2GetRead(const char* InputFile, const char* fileFormat,  unsigned int uiExpReadsStrLength, bool bDiscardReadsWN = false);
    void save_next_read_id(const char* tagLine, char sep = ',');
    CReadsFileParser parser;
    void clearReads(void);
    void removeExtraTags(void);
    void getQualityScoresFromQUAL(void);
    void handleBadRead(void);
    unsigned int getExpReadLength(unsigned int getExpReadLength);
private:
    int initialization(void);
};
#endif
