#pragma once
/*
 * This function is a base clase for CGenome_Index_Table
 * It has an array of genome index, which is binned and sorted into buckets.
 * It has a CHashIndex, which is a hash index array pointing to the first reacord of each bin
 */
#include "HashIndexT.h"
#include "boolFlagArray.h"
#include "chdir.h"
#include "stdafx.h"
#include "seedOptions.h"
#include <string>
#include <iostream>
using std::string;

typedef unsigned int CIndex_Type;

class CIndex_Table
{
public:
    CIndex_Table(void);
    ~CIndex_Table(void);
    void delete_index_table(void);
    // A index array represent the substring of a genome
    CIndex_Type* pIndexTable;
    // A hash index array pointing to the first reacord of each bin
    CHashIndexT* pHashIndexTable;

    // A bit array object indicating the neighborhood seed-pattern record are the same.
    CboolFlagArray* pbaMaskedFlag;
    // Flag array which mark the index as the representative copy of the mathmatical repeats.
    CboolFlagArray* pbaRepeatRepresentativeFlag;

    unsigned int size;
    // unsigned int capacity;
    unsigned int chosenSeedId;
    unsigned int uiSubDiffThreshold;
    unsigned int uiRead_Length;
    bool bMapReadInColors;
protected:
    string indexFileName;
    bool save_Hash_Table(FILE* fp) const;
    bool read_Hash_Table(FILE* fp);
private:
    int initialization(void);
    void printInfo(void) const;
};
string default_index_path(string filePostfix, bool colorReads,\
                          unsigned int seedOption, unsigned int uiReadLength);
