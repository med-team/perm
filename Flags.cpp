#include "Flags.h"
using namespace std;

bool CFlags::checkArg(int argc, const char** argv, const char* arg)
{
    this->flags.push_back(arg);
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], arg) == 0) {
            return(true);
        }
    }
    return(false);
}

bool CFlags::checkIntOpt(int argc, const char** argv, const char* arg, int& argValue)
{
    this->flags.push_back(arg);
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], arg) == 0) {
            if (argv[i+1][0] != '-') {
                argValue = (int) atof(argv[i+1]);
            }
            return(true);
        }
    }
    return(false);
}

// check the given parameters (argv) contain a specific unsigned integer option.
bool CFlags::checkUnIntOpt(int argc, const char** argv, const char* arg, unsigned int& argValue)
{
    this->flags.push_back(arg);
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], arg) == 0) {
            if (argv[i+1][0] != '-') {
                argValue = (unsigned int) atof(argv[i+1]);
            }
            return(true);
        }
    }
    return(false);
}

bool CFlags::checkpCharOpt(int argc, const char** argv, const char* arg, char& argValue)
{
    this->flags.push_back(arg);
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], arg) == 0) {
            int argLength = (int)strlen(argv[i+1]);
            if (argLength == 1) {
                argValue = argv[i+1][0];
                return(true);
            } else if (argLength == 3) {
                argValue = argv[i+1][1];
                return(true);
            } else {
                cout << "Unrecognizable delimiter " << argv[i+1] << "for read id." << endl;
                cout << "Space, tab and comma are the default delimiters." << endl;
                cout << "Put single quote around the delimiter." << endl;
            }
        }
    }
    return(false);
}

// check if the parameters (argv) contain a specific string option.
bool CFlags::checkpStrOpt(int argc, const char** argv, const char* arg, string& argStr)
{
    this->flags.push_back(arg);
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], arg) == 0) {
            if (argv[i+1][0] != '-') {
                argStr = string(argv[i+1]);
            }
            return(true);
        }
    }
    return(false);
}

// check if the parameters (argv) contain a specific string option.
bool CFlags::checkpStrOpt(int argc, const char** argv, const char* arg, char* argStr)
{
    this->flags.push_back(arg);
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], arg) == 0) {
            if (argv[i+1][0] != '-') {
                strcpy(argStr, argv[i+1]);
            }
            return(true);
        }
    }
    return(false);
}

bool CFlags::checkUnrecognizedFlags(int argc, const char ** argv)
{
    for (int i = 1; i < argc; i++) {
        if ( argv[i][0] == '-') {
            bool bRecognized = false;
            for (vector<string>::iterator it = this->flags.begin(); \
                    it != this->flags.end(); it++) {
                if (strcmp(argv[i], it->c_str()) == 0) {
                    bRecognized = true;
                }
            }
            if (bRecognized == false) {
                LOG_INFO("\nInfo %d: unrecognized option %s.\n", INFO_LOG, argv[i] );
            }
        }
    }
    return(true);
}