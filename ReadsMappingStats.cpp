#include "stdafx.h"
#include "ReadsMappingStats.h"

CReadsMappingStats::CReadsMappingStats(void)
{
    this->initialization();
}

CReadsMappingStats::~CReadsMappingStats(void)
{

}

void CReadsMappingStats::initialization(void)
{
    this->iReadsFileCount = 0;
    this->iMultiMappedLocationThreshold = 2000;
    // this->iMultiMappedLocationThreshold = 1000000;
    this->initializeStatsCounter(); // Counters
}

void CReadsMappingStats::initializeStatsCounter(void)
{
    this->iBadReadCounter = 0;
    this->iMapCount = 0;
    this->iReadCounter = 0;
    this->iMissReadCounter = 0;
    this->iMultiMappedReads = 0;
    this->iReadsFileCount = 0;
    this->iReadsMapped2tooManyLocations = 0;

    for (unsigned int i = 0; i <= MAXTOLERATSUBMIS; i++) {
        // Temporarily assume there have same # of record
        this->iMapDiffCount[i] = 0;
    }
}

void CReadsMappingStats::printCommand(ostream& out, string command)
{
    out << command << endl;
}

int CReadsMappingStats::printMappingStats(ostream& out, const char* readSetName,\
        unsigned int uiSubThreshold) const
{
    unsigned int i;
    out << endl;
    out << readSetName << ", Reads:, ";
    out << "Filtered#, " << iBadReadCounter << ", ";
    out << "Kept#, " << iReadCounter << ", ";
    out << "Mapped#, " << iMapCount << ", ";
    out << "Multimapped#, " << iMultiMappedReads << ", ";
    out << "Multimapped>" << iMultiMappedLocationThreshold << "#, ";
    out << iReadsMapped2tooManyLocations << BLANK_LINE << endl;
    out << readSetName << ",_ ";
    for (i = 0; i <= uiSubThreshold; i++) {
        out << "Sub" << i << ", " << iMapDiffCount[i] << ", " ;
    }
    out << endl;
    return(0);
}

// return true if print the alignments
void CReadsMappingStats::bookKeepMapping(CAlignmentsQ& que)
{
    this->iMapCount++;
    this->iMapDiffCount[que.MinDiff]++;
    if (que.AmbiguousFlag) {
        this->iMultiMappedReads++;
    }
}

bool CReadsMappingStats::printAlignmentOrNot(CAlignmentsQ& que, bool bExcludeAmbiguous, bool ambiguousOnly) const
{
    bool ambiguousFlag = false;
    if (que.qAllInThreshold()) {
        // if bExcludeAmbiguous and qAllInThreshold (-E -A), print only if there are single record.
        if (que.load > 1) {
            ambiguousFlag = true;
        }
    } else {
        if (que.AmbiguousFlag) {
            ambiguousFlag = true;
        }
    }

    if (ambiguousFlag) {
        if (ambiguousOnly) {
            return(true);
        } else if (bExcludeAmbiguous) {
            return(false);
        } else {
            return(true);
        }
    } else {
        if (ambiguousOnly) {
            return(false); // If the mapping is not ambiguous but only ambiguous mapping are meant to be printed.
        } else {
            return(true);
        }
    }
}

