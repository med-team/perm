#pragma once
#include <queue>
using namespace std;
class minQ
{
public:
    minQ(void);
    ~minQ(void);
    queue<int> q;
    deque<int> min;
    int push(int num);
    int pop();
    int front();
    int getMin();
};
void testMinQ();
