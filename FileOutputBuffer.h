#pragma once
#include <iostream>
#include "stdafx.h"
/*****
This is a class is used as the output buffer to speed up the output to file
the output will temporarily send to the buffer and output in batch to speed up.
Currently, one need to estimate when to output the buffer before it is overflow
*/
const unsigned int DEFAULT_BUEFFER_SIZE = 1000000;

class FileOutputBuffer
{
//Assume uiCapacity is > 100 and each line is < 100 char
public:
    FileOutputBuffer(unsigned int uiCapacity, ofstream* pofile);
    FileOutputBuffer(void);
    ~FileOutputBuffer(void);
    unsigned int uiCapacity;
    unsigned int uiSize;
    void UpdateSize();
    void fflush();
    void removeEndBlankLine();
    char* caBuffer;
    char* caBufp;
    ofstream* pofile;
};


