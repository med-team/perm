#pragma once
// Maximum number of chromosome in a genome
const unsigned int GENOME_CAPACITY = 64;
#include "chromosomeNTdata.h"
#include "ShortReadUtil.h"
#include "SeedPattern.h"
#include "stdafx.h"
#include <string>
#include <set>
#include <limits>

const unsigned int BAD_GENOME_INDEX = std::numeric_limits<unsigned int>::max();
const int _MAX_READ_LENGTH_ = 1024;

/*
This class is designed to contain a collection of CchromosomeNTdata objects
It will contain the function to convert the chromosome index to genome index, and vice versa
This object is currently set to be a member of Kmer_LSH_table
*/
#include "stdafx.h"
#include "Filename.h"

class CGenomeNTdata
{
public:
    CGenomeNTdata(void);
    CGenomeNTdata(const char* DataSetListFile);
    ~CGenomeNTdata(void);
    // A buffer to store the read when genomelocusID2Kmer is called
    char refName[MAX_LINE];
    char caKmer[_MAX_READ_LENGTH_];

    unsigned int iGenomeSize;//Total base pair in the genome
    unsigned int iNo_of_chromosome;
    // The pointer array to point to each chromosome objects
    CchromosomeNTdata* paChromosomes[GENOME_CAPACITY];

    // delete the spaced used character string of each chromosome
    int freeChromosomeSpace(void);

    /* This function add one more chromosome in the genome set
     (Which is maintained by a pointer array) */
    unsigned int addChromosome(const char* chromosomeFileName, bool bFastaFormat = true);

    // This function will store corresponds kmer in this->caKmer
    char* genomeLocusID2Kmer(unsigned int uiKmer_Length, unsigned int genomeLocusID);

    /* This function will covert the position described by number pair
     * (chromosome ID and local position locus ID)
     * to genome locus index recorded in the table list */
    unsigned int chrIndex2genomelocusID(unsigned int iChrID, unsigned int iChrLocusID);
    unsigned int genomeIndex2chrID(unsigned iGenomeIndex);

    // This function will covert to genome locus index recorded in the table list
    // to the locus index recorded in the table list
    unsigned int genomeLocusID2chrIndex(unsigned int igenomeLocusID);

    // This table records the accumulated number of NT in the genome
    unsigned int IndexCovertTable[GENOME_CAPACITY];

    void checkRefsNames(void);
    vector<CGene> getRefNamesLengths(void);
private:
    int initialization();
};

// For test purpose, exaxhustive search a kmer is in the genome
unsigned int BruteForceSearch(CGenomeNTdata& genome, char* Kmer);

