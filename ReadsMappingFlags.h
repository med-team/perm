#pragma once
#include "stdafx.h"
// #include "ProgramOptions.h"

class CReadsMappingFlags
{
public:
    CReadsMappingFlags(void);
    ~CReadsMappingFlags(void);
protected:
    // int ptr_initialization(CProgramOptions *_param);
    int set_Default_Opt(void);

    //unsigned int uiSubThreshold;
    bool bSaveTable;
    bool bPrintUnMappedReads;
    bool bPrintAlignment;
    bool bPrintGeneName;
    // search not only the best in terms of mismatches, but all within the criteria.
    bool bSearchAllAlignment; // Currently set to be false by default
    char cOutputFormat;
};

