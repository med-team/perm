#pragma once
#include "stdafx.h"
#include "GenomeNTdata.h"
#include "AlignmentsQ.h"

/*
 * This class is a base class for CReadsMapping, which collects the counters for statistics of mapping reads.
 */

class CReadsMappingStats
{
public:
    CReadsMappingStats(void);
    virtual ~CReadsMappingStats(void);
    CAlignmentsQ alignmentsQ[2]; // Queue the founded alignments (Two Queues for paired end reads)
protected:
    static const unsigned int SNP_TYPE_NUM = 4;  // Complement, Transition, Transvertion and Mixed
    int iReadsFileCount;

    /* Basic mapping statistics counter */
    void initializeStatsCounter(void);
    // return true if print the alignments
    void bookKeepMapping(CAlignmentsQ& que);
    // return to a bool value indicating the alignment should be printed or not
    bool printAlignmentOrNot(CAlignmentsQ& que, bool bExcludeAmbiguous, bool ambiguousOnly) const;
    // Print the counter in an order of Runs,Chr0,Chr1,Chr2,Total Hits,Sub0,Sub1,Sub2,Total Kmers
    int printMappingStats(ostream& out, const char* readSetName, unsigned int uiSubThreshold) const;

    void printCommand(ostream& out, string command);

    unsigned int iMapCount;
    unsigned int iMapDiffCount[MAXTOLERATSUBMIS + 1];

    unsigned int iReadCounter;
    unsigned int iBadReadCounter;
    unsigned int iMissReadCounter;
    // # of reads that has multiple place mapping(repeats or ambiguous read)
    unsigned int iMultiMappedReads;
    unsigned int iReadsMapped2tooManyLocations;
    unsigned int iMultiMappedLocationThreshold;
private:
    void initialization(void);
};

