#ifndef SEED_OPTIONS
#define SEED_OPTIONS
const unsigned int FULL_SENSITIVE_OPT_TO_TWO_BASE_MIS = 20;
const unsigned int FULL_SENSITIVE_OPT_TO_ONE_BASE_ONE_COLOR_MIS = 11;
const unsigned int FULL_SENSITIVE_OPT_TO_ONE_BASE_TWO_COLOR_MIS = 12;
const unsigned int MAX_FULL_SENSITIVITY_OPT = 4; // used when allowing more mismatch with partial sensitivity, for higher mismatch threshold.
const unsigned int DEFAULT_SEED_OPTION = 3; // used when allowing more mismatch with partial sensitivity, for higher mismatch threshold.
#include <string>
using namespace std;
inline string seedSymbol(unsigned seedId)
{
    switch (seedId) {
    case 0:
        return(string("F0"));
    case 1:
        return(string("F1"));
    case 2:
        return(string("F2"));
    case 11:
        return(string("S11"));
    case 12:
        return(string("S12"));
    case 3:
        return(string("F3"));
    case 20:
        return(string("S20"));
    case 4:
        return(string("F4"));
    default:
        LOG_INFO("\nInfo %d: Unknown seed opts.\n", INFO_LOG);
        return(string(""));
    }
}
#endif
