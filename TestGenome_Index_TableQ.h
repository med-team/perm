#pragma once
#include "Genome_Index_TableQ.h"

class SimulateLongRead
{
public:
    unsigned int uiReadLength;
    SimulateLongRead(CGenomeInBits* pgenomeNTInBits, unsigned int startIndex);
    ~SimulateLongRead(void);
    bool goodRead;
    CReadInBits half1st;
    CReadInBits half2nd;
    char read[MAX_LINE];
    char originalRead[MAX_LINE];
};

bool testGenome_Index_TableQ(CGenome_Index_TableQ* table);
bool testMappingLongRead(CGenome_Index_TableQ* table);
bool testMappingLongPairedRead(CGenome_Index_TableQ* table);

