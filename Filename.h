#ifndef FILENAME_H
#define FILENAME_H

#include <string>
#include <ctype.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>  // For stat().
#include <sys/stat.h>   // For stat().
using namespace std;
const int MAX_FILE_PATH = 2048;

string chExtName(string filename, string Extname);
string getBasename(const char* Path);
int chExtName(char* filename, const char* Extname);
int getBasename(const char* Path, char* fileNameWithoutExt); // no ext name
const char* getPtrBasename(const char* Path); // return the pointer of the start
int getTitleFromPath(const char* Path, char* Title);
string getFullPath(string directory, string filename);
char* addPath(const char* filename, const char* folder, char* path);
const char* getExtName(const char* filename);
bool hasTheExtName(const char* filename, const char* extFile);
bool fileExist(const char* filename);
bool checkFileExist(const char* filename);
bool checkPathCharsAreValid(const char* pathStr);
// (Modify a string into a valid filename by replacing its invalid characters)
void filenameLize(char* string);
bool dirExist(const char* strFolderPath);
bool isPathWritable(const char* filename);
/*
 * Get the filename from the next line of a filelist list.
 * If there are no more files, put "\0" in the char* and return false
 */
bool GetNextFilenameFromListFile(ifstream &ifile, char* filenameBuffer);
/*
 * Get the filename paired from the next line of a filelist list.
 * If there are no more files, put "\0" in the char* and return false
 */
bool GetNextFilenamePairFromListFile(ifstream &ifile, char* filenameBuffer1, char* filenameBuffer2);
bool GetNextFilenamePairFromListFile(const char* filename, char* filenameBuffer1, char* filenameBuffer2);
#endif


